﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    var standardObstaclesVelocity = 5;
    var distanceMeanBetweenObstacles = 50;
    var distanceVarianceBetweenObstacles = 40;

    var distanceTarget;
    var distanceReached;
    var jumpCoordinatesY;
    var currentJumpPhase;
    const JUMP_PHASES = 24;

    document.addEventListener('deviceready', onDeviceReady, false);
    document.addEventListener('touchstart', onTouch, false);

    function onDeviceReady(event) {
        document.addEventListener('pause', onPause.bind( this ), false );
        document.addEventListener('resume', onResume.bind(this), false);

        distanceTarget = generateGaussianDistributionNumber(distanceMeanBetweenObstacles, distanceVarianceBetweenObstacles);
        distanceReached = 0;

        createStudent();
        createStudentJumpFunctionality();
    };

    function onTouch(event) {
        event.preventDefault();
        var touchObj = event.changedTouches[0];
        var coordinateX = parseInt(touchObj.pageX);
        if (coordinateX < screen.width / 2) {
            currentJumpPhase = 0;
        } else {
            fire();
        }
    }

    function onPause() {
    };

    function onResume() {
    };

    // Start things off
    requestAnimationFrame(mainLoop);

    function mainLoop() {
        checkJumpingState();
        executeStandardMove(getAllObstacles());
        clearOutsiders(getAllObstacles());
        obstaclesGenerator();
        setTimeout(function () {
            requestAnimationFrame(mainLoop);
        }, 20);
    }

    function executeStandardMove(obstacles) {
        for (var i = 0; i < obstacles.length; i++) {
            obstacles[i].style.left = parseInt(obstacles[i].style.left) - standardObstaclesVelocity + 'px';
            obstacles[i].style.right = parseInt(obstacles[i].style.left) + screen.width / 5 + 'px';
        }
    }

    function obstaclesGenerator() {
        if (distanceReached == distanceTarget) {
            createObstacle();
            distanceTarget = generateGaussianDistributionNumber(distanceMeanBetweenObstacles, distanceVarianceBetweenObstacles);
            distanceReached = 0;
        } else {
            distanceReached++;
        }
    }

    function clearOutsiders(obstacles) {
        for (var i = 0; i < obstacles.length; i++) {
            if (parseInt(obstacles[i].style.right) < 0) {
                document.body.removeChild(obstacles[i]);
            }
        }
    }

    function getAllObstacles() {
        return document.getElementsByClassName('obstacle');
    }

    function createObstacle() {
        var div = document.createElement('div');
        document.body.appendChild(div);
        div.className = 'obstacle';
        div.style.left = screen.width + 'px';
        div.style.right = screen.width + screen.width / 9 + 'px';
        div.style.width = screen.width / 9 + 'px';
        div.style.bottom = '0px';
        div.style.height = screen.height / 7 + 'px';
    }

    function createStudent() {
        var div = document.createElement('div');
        document.body.appendChild(div);
        div.className = 'student';
        div.style.left = screen.width / 10 + 'px';
        div.style.right = screen.width / 10 + screen.width / 10 + 'px';
        div.style.width = screen.width / 10 + 'px';
        div.style.bottom = '0px';
        div.style.height = screen.height / 8 + 'px';
    }

    function createStudentJumpFunctionality() {
        jumpCoordinatesY = [
            parseInt(screen.height / 50),
            parseInt(screen.height / 47),
            parseInt(screen.height / 45),
            parseInt(screen.height / 42),
            parseInt(screen.height / 39),
            parseInt(screen.height / 36),
            parseInt(screen.height / 34),
            parseInt(screen.height / 32),
            parseInt(screen.height / 31),
            parseInt(screen.height / 31),
            parseInt(screen.height / 30),
            parseInt(screen.height / 30),
            parseInt(screen.height / -30),
            parseInt(screen.height / -30),
            parseInt(screen.height / -31),
            parseInt(screen.height / -31),
            parseInt(screen.height / -32),
            parseInt(screen.height / -34),
            parseInt(screen.height / -36),
            parseInt(screen.height / -39),
            parseInt(screen.height / -42),
            parseInt(screen.height / -45),
            parseInt(screen.height / -47),
            parseInt(screen.height / -50)
        ];
    }

    function jump(progress) {
        document.getElementsByClassName('student')[0].style.bottom = parseInt(document.getElementsByClassName('student')[0].style.bottom) + jumpCoordinatesY[progress] + 'px';
    }

    function checkJumpingState() {
        if (currentJumpPhase < JUMP_PHASES) {
            jump(currentJumpPhase);
            currentJumpPhase++;
        }
    }

    function generateGaussianDistributionNumber(mean, variance) {
        if (mean == undefined)
            mean = 0.0;
        if (variance == undefined)
            variance = 1.0;
        var V1, V2, S;
        do {
            var U1 = Math.random();
            var U2 = Math.random();
            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
        } while (S > 1);

        var X = Math.sqrt(-2 * Math.log(S) / S) * V1;
        X = mean + Math.sqrt(variance) * X;

        return Math.floor(X);
    }
} )();